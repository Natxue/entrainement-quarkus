package org.acme.materiel;

import org.acme.materiel.MaterielEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterielRepository extends JpaRepository<MaterielEntity, Integer>{
}
