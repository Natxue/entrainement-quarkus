package org.acme.materiel;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/Materiel")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MaterielResource {

    private MaterielService service;

    @Inject
    public MaterielResource(MaterielService materielService){
        this.service=materielService;
    }

    @GET
    @Path("/lecture")
    public List<MaterielEntity> listAll(){
        return this.service.getAllMateriels();
    }

    @POST
    @Path("/creation")
    public List<MaterielEntity> add(@Valid MaterielEntity materiel){
        this.service.newMateriel(materiel);
        return this.service.getAllMateriels();
    }

    @PUT
    @Path("/modification")
    public MaterielEntity update(@Valid MaterielEntity materiel){
        return this.service.updateMateriel(materiel);
    }

    @DELETE
    @Path("/suppression/{id}")
    public void delete(@PathParam("id") int id){
        this.service.deleteMateriel(id);
    }
}
