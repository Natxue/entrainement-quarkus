package org.acme.materiel;

import org.acme.materiel.MaterielEntity;
import org.acme.materiel.MaterielRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class MaterielService {

    @Inject
    MaterielRepository repository;

    //lecture
    public List<MaterielEntity> getAllMateriels(){
        return repository.findAll();
    }

    //creation
    @Transactional
    public MaterielEntity newMateriel(MaterielEntity nouveauMat){
        repository.save(nouveauMat);
        return nouveauMat;
    }

    //modification
    @Transactional
    public MaterielEntity updateMateriel(MaterielEntity materiel){
        if(repository.findById(materiel.getId()).isPresent()){
            return this.repository.save(materiel);
        }
        return null;
    }

    //suppression
    @Transactional
    public void deleteMateriel(int materielID){
        repository.deleteById(materielID);
    }
}
