package org.acme.categorie;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name="categorie")
public class CategorieEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "categorie_id", nullable = false)
    private int id;

    private String nom;

    @Column(name = "updated_at")
    private LocalDate update;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getUpdate() {
        return update;
    }

    public void setUpdate(LocalDate update) {
        this.update = update;
    }

    @Override
    public String toString() {
        return "CategorieEntity{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", update=" + update +
                '}';
    }
}
