package org.acme.categorie;

import org.acme.materiel.MaterielEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class CategorieService {

    @Inject
    CategorieRepository repository;


    //ajout
    @Transactional
    public CategorieEntity newCategorie(CategorieEntity nouvelleCat){
        repository.save(nouvelleCat);
        return nouvelleCat;
    }

    public List<CategorieEntity> getAll(){
        return repository.findAll();
    }
}
