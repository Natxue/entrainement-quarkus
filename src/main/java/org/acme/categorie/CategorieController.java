package org.acme.categorie;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/Categorie")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategorieController {

    private CategorieService service;

    @Inject
    public CategorieController(CategorieService categorieService){
        this.service = categorieService;
    }

    @POST
    @Path("/ajout")
    public List<CategorieEntity> ajout(@Valid CategorieEntity categorie){
        this.service.newCategorie(categorie);
        return this.service.getAll();
    }

    @GET
    @Path("/affichage")
    public List<CategorieEntity> affichage(){
        return this.service.getAll();
    }


}
